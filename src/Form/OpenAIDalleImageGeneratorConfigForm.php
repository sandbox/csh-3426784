<?php

namespace Drupal\openai_dalle_image_generator\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures the OpenAI DALL-E Image Generator settings.
 */
class OpenAIDalleImageGeneratorConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'openai_dalle_image_generator.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'openai_dalle_image_generator_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('openai_dalle_image_generator.settings');

    $form['openai_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OpenAI API Key'),
      '#default_value' => $config->get('openai_api_key')??'',
      '#description' => $this->t('Enter your OpenAI API key.'),
      '#required' => TRUE,
    ];

    $form['openai_api_url'] = [
      '#type' => 'url',
      '#title' => $this->t('OpenAI API URL'),
      '#default_value' => $config->get('openai_api_url')??'https://api.openai.com/v1/images/generations',
      '#description' => $this->t('Enter the OpenAI API URL.'),
      '#required' => TRUE,
    ];

    $form['openai_api_model'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OpenAI API Model'),
      '#default_value' => $config->get('openai_api_model')??'dall-e-3',
      '#description' => $this->t('Enter the OpenAI API model to use.'),
      '#required' => TRUE,
    ];

    $form['openai_api_prompt'] = [
      '#type' => 'textarea',
      '#title' => $this->t('OpenAI API Prompt'),
      '#default_value' => $config->get('openai_api_prompt')??'A painting of a cat',
      '#description' => $this->t('Enter the prompt for the image generation.'),
      '#required' => TRUE,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('openai_dalle_image_generator.settings')
      ->set('openai_api_key', $form_state->getValue('openai_api_key'))
      ->set('openai_api_url', $form_state->getValue('openai_api_url'))
      ->set('openai_api_model', $form_state->getValue('openai_api_model'))
      ->set('openai_api_prompt', $form_state->getValue('openai_api_prompt'))
      ->save();
  }

}
