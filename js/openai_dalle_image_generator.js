(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.openaiDalleImageGenerator = {

    attach: function (context, settings) {
      // Get the configuration values from Drupal settings.
    //Uncaught TypeError: Cannot read properties of undefined (reading 'openai_api_key'), sometimes this error occurs when the settings are not available yet.
    //To avoid this error, we can use the context parameter to check if the settings are available.

      if (typeof settings.openai_dalle_image_generator === 'undefined') { return; }
      const apiKey = settings.openai_dalle_image_generator.openai_api_key;
      const apiUrl = settings.openai_dalle_image_generator.openai_api_url;
      const apiModel = settings.openai_dalle_image_generator.openai_api_model;
      const apiPrompt = settings.openai_dalle_image_generator.openai_api_prompt;

      // Call the OpenAI DALL-E API to generate the image.
      $.ajax({
        url: apiUrl,
        type: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${apiKey}`
        },
        data: JSON.stringify({
          'model': apiModel,
          'prompt': apiPrompt
        }),
        success: function(response) {
          // Handle the successful response from the API.
          const imageUrl = response.data[0].url;
          const imageCreatedByAiUrl = Drupal.url('openai_dalle_image_generator.image_created_by_ai', { query: { image_url: imageUrl } });
          window.location.href = imageCreatedByAiUrl;
        },
        error: function(xhr, status, error) {
          // Handle the error response from the API.
          console.error(error);
        }
      });
    }
  };

})(jQuery, Drupal);
